//
//  DetailTableViewCell.swift
//  WeatherApp
//
//  Created by Sergii on 3/8/18.
//  Copyright © 2018 Sergiy. All rights reserved.
//

import UIKit
import SDWebImage

class DetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iconWeatherImageView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var indicatorImageView: UIImageView!
    
    private var weather: WeatherApi?
    
    func setup(cityIndex: Int) {
        
        activityIndicatorView.startAnimating()
        let cities = DataManager.shared.getSities()
        DataManager.shared.getWeatherDetail(cityName: cities[cityIndex] ) { (weather, error) in
            if let weather = weather {
                self.weather = weather
            }
            DispatchQueue.main.async{
                self.activityIndicatorView.stopAnimating()
                self.setupData()
            }
        }
    }

    private func setupData() {
        
        if let cityName = weather?.name {
            cityNameLabel.text = cityName
        }
        
        if let iconName = weather?.icon {
            let iconURL = DataManager.shared.getWeatherUrl(name: iconName)
            iconWeatherImageView.sd_setImage(with: iconURL)
        }
        
        if let temp = weather?.temp {
            let celsium = temp.convertToCelsius()
            indicatorImageView.tintColor = UIColor().temperatureIndicator(value: celsium)
            tempLabel.text? = "\(celsium)°"
        }   
    }
}
