//
//  WeatherModel.swift
//  WeatherApp
//
//  Created by Sergii on 3/7/18.
//  Copyright © 2018 Sergiy. All rights reserved.
//

import Foundation

fileprivate struct RawWeatherResponse: Decodable {
    
    struct Main: Decodable {
        var temp: Float
    }
    
    struct Weather: Decodable {
        var icon: String
    }
    
    var name: String
    var weather: [Weather]
    var main: Main
}

struct WeatherApi: Codable {
    
    var name: String
    var icon: String
    var temp: Float
   
    init(from decoder: Decoder) throws {
        let rawResponse = try RawWeatherResponse(from: decoder)
        name = rawResponse.name
        icon = (rawResponse.weather.first?.icon)!
        temp = rawResponse.main.temp
    }
}


