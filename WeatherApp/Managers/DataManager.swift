//
//  DataManager.swift
//  WeatherApp
//
//  Created by Sergii on 3/7/18.
//  Copyright © 2018 Sergiy. All rights reserved.
//

import Foundation

class DataManager {
    
    static let shared = DataManager()
    
    private let apiURL = "https://api.openweathermap.org/data/2.5/weather"
    private let apiKey = "d2bd923726d8850b7677856f80cb52cd"
    
    private let apiImgUrl = "https://openweathermap.org/img/w/"
    private let imgExtention = ".png"
    
    private init() {}
    
    func getSities() -> [String] {
        return ["Kiev", "Kharkiv", "Dnipro", "Donetsk", "Odesa", "Zaporizhzhya", "Lviv", "Khmelnytskyy", "Mykolayiv", "Mariupol"]
    }
    
    func getWeatherDetail(cityName: String, callback:@escaping (_ result: WeatherApi?, _ error: Error?)-> Void) {
        
        let path = apiURL + "?q=\(cityName)&appid=\(apiKey)"
        let url = URL(string: path)
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            guard let data = data else { return }
            do {
                let gitData = try JSONDecoder().decode(WeatherApi.self, from: data)
                callback(gitData, nil)
            } catch let err {
                callback(nil, err)
            }
        }
        task.resume()
    }
    
    func getWeatherUrl(name: String) -> URL {
        let iconName = name + imgExtention
        let path = apiImgUrl + iconName
        return URL(string: path)!
    }
}
