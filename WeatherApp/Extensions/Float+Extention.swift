//
//  String+Extention.swift
//  WeatherApp
//
//  Created by Sergii on 3/8/18.
//  Copyright © 2018 Sergiy. All rights reserved.
//

import Foundation

public extension Float {
    
    func convertToCelsius() -> Int {
        return lroundf(self - 273.15)
    }
}
