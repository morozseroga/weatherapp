//
//  ViewController.swift
//  WeatherApp
//
//  Created by Sergii on 3/7/18.
//  Copyright © 2018 Sergiy. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    var dataSource: [String]!
    var valueToPass:String!
    let detailSegueIdentifier = "showDetailSegue"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Cities"
        dataSource = DataManager.shared.getSities()
        tableView.backgroundView = UIImageView(image: UIImage(named: "bg"))
    }
    
    // MARK: - UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cityCell", for: indexPath)
        cell.textLabel?.text = dataSource[indexPath.row]

        return cell
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? UITableViewCell, segue.identifier == detailSegueIdentifier {
            let vc = segue.destination as! DetailViewController
            let indexPath = tableView.indexPath(for: cell)
            vc.cityIndex = indexPath?.row
        }
    }
}

