//
//  DetailViewController.swift
//  WeatherApp
//
//  Created by Sergii on 3/7/18.
//  Copyright © 2018 Sergiy. All rights reserved.
//

import UIKit
import Reachability

class DetailViewController: UIViewController {

    var cityIndex: Int!
    var cityArray = [String]()
    var weather: WeatherApi?

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var errorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Detail"
        
        cityArray = DataManager.shared.getSities()
        
        pageControl.numberOfPages = cityArray.count
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.performBatchUpdates({}) { (finished) in
            self.pageControl.currentPage = self.cityIndex
            self.scrollTo(index: self.cityIndex, animated: false)
        }
        
        let reac = ReachabilityManager.shared
        reac.completion = {isConnect in
            self.errorView.isHidden = isConnect
            if isConnect {
                self.collectionView.reloadData()
            }
        }
    }
    
    private func scrollTo(index: Int, animated: Bool){
        collectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .centeredHorizontally, animated: animated)
    }
    
    //MARK - Actions
    @IBAction func changeCell(_ sender: UIPageControl) {
        scrollTo(index: sender.currentPage, animated: true)
    }
}

extension DetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cityArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.init(describing: DetailCollectionViewCell.self), for: indexPath) as! DetailCollectionViewCell
        cell.setup(cityIndex: indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageControl.currentPage = indexPath.row
    }
}
