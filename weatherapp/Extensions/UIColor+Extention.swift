//
//  UIColor+Extention.swift
//  WeatherApp
//
//  Created by Сергей Мороз on 08.03.18.
//  Copyright © 2018 Sergiy. All rights reserved.
//

import UIKit

public extension UIColor {
    
    func temperatureIndicator(value: Int) -> UIColor {
        
        switch value {
        case -40 ..< -20:
            return .blue
        case -20 ..< 0:
            return .white
        case 0 ..< 20:
            return .green
        case 20 ..< 40:
            return .red
        default:
            return .black
        }
    }
}
