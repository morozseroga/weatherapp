//
//  ReachabilityManager.swift
//  WeatherApp
//
//  Created by Сергей Мороз on 08.03.18.
//  Copyright © 2018 Sergiy. All rights reserved.
//

import UIKit
import Reachability


class ReachabilityManager: NSObject {
    
    static let shared = ReachabilityManager()
    var reach: Reachability?
    var completion : ((_ isConnection: Bool) -> Void)!
    
    private override init() {
        super.init()
        
        configure()
    }
    
    func configure() {
        self.reach = Reachability.forInternetConnection()
        self.reach!.reachableOnWWAN = false
        NotificationCenter.default.addObserver( self,
                                                selector: #selector(reachabilityChanged),
                                                name: NSNotification.Name.reachabilityChanged,
                                                object: nil
        )
        self.reach!.startNotifier()
    }
    
    @objc func reachabilityChanged(notification: NSNotification) {
        if self.reach!.isReachableViaWiFi() || self.reach!.isReachableViaWWAN() {
            completion(true)
        } else {
            completion(false)
        }
    }
}

